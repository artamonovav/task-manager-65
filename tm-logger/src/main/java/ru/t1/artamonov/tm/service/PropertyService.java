package ru.t1.artamonov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['mongo.host']}")
    private String hostName;

    @NotNull
    @Value("#{environment['mongo.port']}")
    public Integer port;

    @NotNull
    @Value("#{environment['mongo.dbname']}")
    public String dbName;

    @NotNull
    @Value("#{environment['broker.url']}")
    public String brokerUrl;

    @NotNull
    @Value("#{environment['broker.queue']}")
    public String brokerQueue;

}
