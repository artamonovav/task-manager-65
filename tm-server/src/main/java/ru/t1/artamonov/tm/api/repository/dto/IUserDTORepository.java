package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.dto.model.UserDTO;

@Repository
public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(String login);

    @Nullable
    UserDTO findByEmail(String email);

}
