package ru.t1.artamonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.api.endpoint.*;
import ru.t1.artamonov.tm.api.service.ILoggerService;
import ru.t1.artamonov.tm.api.service.IPropertyService;
import ru.t1.artamonov.tm.api.service.IServiceLocator;
import ru.t1.artamonov.tm.api.service.ITokenService;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.exception.system.ArgumentIsNullException;
import ru.t1.artamonov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.artamonov.tm.listener.AbstractListener;
import ru.t1.artamonov.tm.util.SystemUtil;
import ru.t1.artamonov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @Nullable
    private AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null) throw new ArgumentIsNullException();
        @Nullable final AbstractListener abstractListener = getListenerByArgument(argument);
        if (abstractListener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(abstractListener.getName()));
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        try {
            processArgument(arg);
        } catch (@NotNull final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) exitApplication();
        prepareStartup();
        processInput();
    }

    private void exitApplication() {
        System.exit(0);
    }

    private void processInput() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
